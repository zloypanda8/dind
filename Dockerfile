FROM debian:9 as build

RUN apt update && apt install -y cowsay
ENTRYPOINT ["cowsay", "Juneway"]
